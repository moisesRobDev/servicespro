package com.example.proyectoapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectoapp.Adapters.ServicesAdapter;
import com.example.proyectoapp.Auth.FirebaseAuthManager;
import com.example.proyectoapp.Model.Account;
import com.example.proyectoapp.Model.Services;
import com.example.proyectoapp.Storage.FirebaseStorageManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class ServicesListActivity extends AppCompatActivity {
    private static final String FSC_SERVICES = "services" ;
    private static final int MY_PERMISSIONS_LOCATION = 10245; //codigo de permisos para la ubicacion
    // Init recyclerView
    private RecyclerView recyclerView;
    private RecyclerView.Adapter serviceAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Services> servicesData;
    private FirebaseStorageManager fs;
    private ProgressBar pBar;

    private FirebaseAuth firebase_auth;
    private FirebaseFirestore firebase_firestore;
    private FirebaseFirestore database;
    private LatLng current_location;
    private FusedLocationProviderClient fusedLocationClient;
    private LatLng coordinates;
    private FirebaseAuthManager fAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);
        setTitle("Servicios");
        this.pBar = (ProgressBar) findViewById(R.id.pBar_id);
        this.fs = new FirebaseStorageManager();
        this.database = FirebaseFirestore.getInstance();
        this.firebase_auth = FirebaseAuth.getInstance();
        this.fAuth = new FirebaseAuthManager();
        initComponents();
        this.getPermissionsLocation();
    }

    /**
     *
     * Inicializamos los componentes.
     *
     */
    public void initComponents(){
        this.servicesData = new ArrayList<Services>();
        this.recyclerView = (RecyclerView) findViewById(R.id.services_list_id);

        // Mejorar el rendimiento del componente
        this.recyclerView.setHasFixedSize(true);

        //Administrador de diseño lineal
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(this.layoutManager);

        this.fs.getCollection(FSC_SERVICES,task -> {
            if(task.isSuccessful()){
                for(DocumentSnapshot document : task.getResult()){
                    servicesData.add(new Services(document.get("name").toString(),document.get("description").toString()));
                }
                this.serviceAdapter = new ServicesAdapter(this,servicesData);
                this.recyclerView.setAdapter(this.serviceAdapter);
                this.pBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu m){
        getMenuInflater().inflate(R.menu.main_menu,m);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem mI){
        switch (mI.getItemId()){
            case R.id.logout:
                this.fAuth.logout();
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                finish();
                return true;
            case R.id.activa_ubicacion:
                setFinderTrue();
                return true;
            default:
                return super.onOptionsItemSelected(mI);
        }
    }
    /**
     *
     * Esta funcion permite actualizar las coordenadas en la base de datos y a su vez permite paasar el estado de activacion a true
     * lo que hace que las demás personas lo puedan ubicar segun su profeson.
     *
     */

    public void setFinderTrue(){

        /**
         *
         * Añadir aca la funcionalidad de activar la ubicacion en el mapa lo que hay que hacer es actualizar el documento en la base dae datos,
         * colocar find = true y asignar las coordenadas.
         *
         * test 1.
         *
         */
        FirebaseAuth aut = this.firebase_auth;
        FirebaseFirestore database = this.database;

        Toast toast = Toast.makeText(this , "Se ha actualzado su ubicación", Toast.LENGTH_SHORT);

        this.database.collection("account")
                .whereEqualTo( "email" , this.firebase_auth.getCurrentUser().getEmail() )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            System.out.println("email " + aut.getCurrentUser().getEmail() );
                            Account account = new Account( );
                            account.setProfile_imagen( document.getString( "profile_imagen") );
                            account.setProfesion( document.getString( "profesion") );
                            account.setPassword( document.getString( "password") );
                            account.setPhone( document.getString( "phone") );
                            account.setName( document.getString( "name") );
                            account.setEmail( document.getString( "email") );
                            account.setLast_name( document.getString( "last_name") );
                            account.setFind( true );
                            updateCurrentLocation( account , document );
                            toast.show();
                        }
                    }
                });


    }

    /**
     *
     * Trae la ubicación actual
     * */
    public void updateCurrentLocation( Account account , QueryDocumentSnapshot document){
        final LatLng coordinates = this.coordinates;
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                account.setCoordinates( new LatLng(location.getLatitude(),location.getLongitude()));
                database.collection("account").document( document.getId() ).set( account )
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                System.out.println("se guardo");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        System.out.println("error " +e);
                    }
                });
            }
        });
    }


    /**
     *
     * Solicita la ubicación actual al inciiar la app
     *
     */

    public void getPermissionsLocation()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast toast = new Toast(this);
                toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_LOCATION);

            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast toast = new Toast(this);
                    toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);
                } else {
                    Toast toast = new Toast(this);
                    toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


}
