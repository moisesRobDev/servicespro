package com.example.proyectoapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.proyectoapp.Auth.FirebaseAuthManager;

public class SplashScreen extends AppCompatActivity {
    private FirebaseAuthManager fsAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
        this.fsAuth = new FirebaseAuthManager();
        isLoging();

    }

    public void isLoging(){
        if(this.fsAuth.isLogin()){
            // Activity services
            goToServices();
        }else{
            goToLogin();
        }
    }

    public void goToLogin(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToServices(){
        Intent intent = new Intent(this,ServicesListActivity.class);
        startActivity(intent);
        finish();
    }

}
