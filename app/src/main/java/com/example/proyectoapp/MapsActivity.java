package com.example.proyectoapp;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyectoapp.Adapters.ServicesAdapter;
import com.example.proyectoapp.Model.Services;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private static final int MY_PERMISSIONS_LOCATION = 10245; //codigo de permisos para la ubicacion
    private FirebaseFirestore database;
    public static final String EMAIL = "com.example.myfirstapp.MESSAGE"; //Para enviar el email a la ficha del contacto
    private String name_service; //para recibir el nombre del servicio deseado
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        this.database = FirebaseFirestore.getInstance();
        Intent intent = getIntent();
        this.name_service = intent.getStringExtra(ServicesAdapter.NAME_SERVICE); //recibimos el nombre del servicio

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        List<LatLng> ubications = new ArrayList<>(); //para guardar las ubicaciones
        CameraPosition newCamPos = new CameraPosition(new LatLng(-33.4426801,-70.6269423),10.5f,mMap.getCameraPosition().tilt,mMap.getCameraPosition().bearing);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast toast = new Toast(this);
                toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_LOCATION);

            }
        } else {
            // aca el codigo cuando los permisos han sido otorgados
            mMap.setMyLocationEnabled( true );


        }

        this.database.collection("account")
                .whereEqualTo("find", true)
                .whereEqualTo("profesion", this.name_service )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        int indice = 0 ; //cuenta las iteraciones de los socumentos que llegan o se reccoren
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                //GeoPoint geo_point = document.getGeoPoint("coordinates");
                                //double lat = geo_point.getLatitude();
                                //double lng = geo_point.getLongitude ();
                                double lat = document.getDouble("coordinates.latitude");
                                double lng = document.getDouble("coordinates.longitude");
                                LatLng latitud_longitud = new LatLng(lat, lng);
                                mMap.addMarker(new MarkerOptions().position( latitud_longitud ).title(  document.getString("email") ) );

                                mMap.setOnMarkerClickListener( new GoogleMap.OnMarkerClickListener(){

                                    @Override
                                    public boolean onMarkerClick(Marker marker) {
                                            viewProfessional( marker.getTitle() );
                                            return false;
                                    }

                                });

                            }
                        } else {
                            System.out.println( "Error al traer el documento ");
                        }
                    }
                });





    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast toast = new Toast(this);
                    toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);
                } else {
                    Toast toast = new Toast(this);
                    toast.makeText(this, " Esta app necesita los permisos para usar el mapa ", Toast.LENGTH_LONG);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    /*
    para abrir la actividad que muestra la ficha del contacto o profesional
     */
    public void viewProfessional( String email) {
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra(EMAIL, email);
        startActivity(intent);
    }
}
