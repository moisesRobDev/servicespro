package com.example.proyectoapp.Auth;

import android.app.Activity;

import com.example.proyectoapp.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthManager {
    public FirebaseAuth auth_firebase;

    // Pasamos la instancia del firebase a la variable auth_firebase;
    public FirebaseAuthManager(){
        this.auth_firebase = FirebaseAuth.getInstance();
    }
    // Comprobamos si inicio sesion
    public boolean isLogin(){
        return this.auth_firebase.getCurrentUser() != null;
    }
    // Obtenemos los datos del usuario actual
    public User getCurrentUser(){
        FirebaseUser current_user = this.auth_firebase.getCurrentUser();
        if(current_user == null){
            return null;
        }
        return new User(current_user.getDisplayName(),current_user.getEmail());
    }
    // Función para loguear al usuario
    public void login(Activity context, String email, String password, OnCompleteListener<AuthResult> responseHandler) {
        if(email.isEmpty() || password.isEmpty()) {
            return;
        }
        this.auth_firebase.signInWithEmailAndPassword(email, password).addOnCompleteListener(context, responseHandler);
    }
    public void logout(){this.auth_firebase.signOut();}
}
