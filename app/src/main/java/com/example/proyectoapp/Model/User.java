package com.example.proyectoapp.Model;

public class User {
    private String name;
    private String email;

    public User(String name,String email){
        this.name = name;
        this.email = email;
    }

    // Getter  y setters

    public String getName(){ return this.name; }
    public String getEmail(){ return this.email; }
    public void setName(String name){ this.name = name;}
    public void setEmail(String email){this.email = email; }
}
