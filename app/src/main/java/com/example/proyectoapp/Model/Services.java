package com.example.proyectoapp.Model;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class Services{
    private String name;
    private String description;
    private String image;

    /**
     * Constructores
     * */
    public Services(String name,String description){
        this.name = name;
        this.description = description;
    }
    public Services(String name,String description,String image){
        this.name = name;
        this.description = description;
        this.image = description;
    }

    public String getName(){ return this.name; }
    public String getDescription(){ return this.description; }
    public String getImage(){ return this.image; }
    public void setName(String name){ this.name = name; }
    public void setDescription(String description){ this.description = description; }
    public void setImage(String image){ this.image = image; }

    @NonNull
    @Override
    public String toString(){
        return this.name;
    }
}

