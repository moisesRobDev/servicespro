package com.example.proyectoapp.Model;


import com.google.android.gms.maps.model.LatLng;

public class Account {
    private String email;
    private String phone;
    private String name;
    private String last_name;
    private String password;
    private String profesion;
    private String type;
    private Boolean find;
    private LatLng coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getFind() {
        return find;
    }

    public void setFind(Boolean find) {
        this.find = find;
    }

    public LatLng getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatLng coordinates) {
        this.coordinates = coordinates;
    }


    public String getProfile_imagen() {
        return profile_imagen;
    }

    public void setProfile_imagen(String profile_imagen) {
        this.profile_imagen = profile_imagen;
    }

    private String profile_imagen; //solo se guarda el nombre de la imagen como un UID


    /*
    *   inicializamos la base de datos de firebase en el consttructor
    */
    public Account(){
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

}
