package com.example.proyectoapp.Storage;

import android.app.ProgressDialog;
import android.net.Uri;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

public class FirebaseStorageManager {
    public int  PICK_IMAGE_REQUEST = 111;
    public Uri file_path;
    private ProgressDialog progress_dialog; // modal con circulo de progreso
    private ImageView imagen_view; // ??????
    private FirebaseStorage storage; // crea una instancia de firebase
    private FirebaseFirestore fs;
    public StorageReference storage_ref;

    public  FirebaseStorageManager(  )
    {
        this.storage = FirebaseStorage.getInstance();
        this.fs = FirebaseFirestore.getInstance();
        this.storage_ref = storage.getReferenceFromUrl("gs://aplicacion-servicios.appspot.com"); // crea una referencia a la base de datos
    }

    /*
    se usa para subir los archivos al storage de google
    a esta funcion se le debe pasar el id del usuario creado en el modelo account
     */
    public UploadTask uploadImage( String _id ){

        StorageReference childRef = this.storage_ref.child( _id+".jpg"); //concatenamos el id del usiaroo con el tipo de imagen
        //uploading the image
        UploadTask uploadTask = childRef.putFile(this.file_path);

        return uploadTask;

    }

    /**
     * Obtener documentos de una coleccion de datos
     * */

    public void getCollection(String path, OnCompleteListener<QuerySnapshot> response){
        this.fs.collection(path).get().addOnCompleteListener(response);
    }
}
