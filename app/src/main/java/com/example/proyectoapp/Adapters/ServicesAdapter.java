package com.example.proyectoapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectoapp.MapsActivity;
import com.example.proyectoapp.Model.Services;
import com.example.proyectoapp.R;

import java.util.ArrayList;

public class ServicesAdapter extends  RecyclerView.Adapter<ServicesAdapter.ServiceViewHolder>{
    final private ArrayList<Services> servicesData;
    final private Context mContext;
    public static final String NAME_SERVICE = "com.example.myfirstapp.MESSAGE";

    /* Inicializamos el adaptador */
    public ServicesAdapter(Context context,ArrayList<Services> servicesData){
        this.mContext = context;
        this.servicesData = servicesData;
    }

    /* Creamos una nueva vista invocando el administrador de diseño */


    public ServicesAdapter.ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.services_list_item,parent,false);
        ServiceViewHolder svh = new ServiceViewHolder(v);
        return svh;
    }

    /* Reemplazamos el contenido invonando el administrador de diseño*/
    public void onBindViewHolder(ServiceViewHolder holder,final int position){
        holder.name.setText(this.servicesData.get(position).getName());
        holder.description.setText(this.servicesData.get(position).getDescription());
        /**
         * Event Click Listener
         * */
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapsActivity.class);
                intent.putExtra(NAME_SERVICE,servicesData.get(position).getName() );
                mContext.startActivity(intent);
            }
        });
    }

    /* Obtener total */
    public int getItemCount(){
        return this.servicesData.size();
    }

    /* Proporcione una referencia a las vistas para cada elemento de datos
    Los elementos de datos complejos pueden necesitar más de una vista por elemento, y
    proporciona acceso a todas las vistas para un elemento de datos en un titular de vista */

    public static class ServiceViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public TextView description;

        public ServiceViewHolder(View v){
            super(v);
            this.name = v.findViewById(R.id.name_service_id);
            this.description = v.findViewById(R.id.description_service_id);
        }

    }


}
