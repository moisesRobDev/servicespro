package com.example.proyectoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.proyectoapp.Auth.FirebaseAuthManager;
import com.example.proyectoapp.Model.Account;
import com.example.proyectoapp.Model.Services;
import com.example.proyectoapp.Storage.FirebaseStorageManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.UUID;

public class SignUpActivity extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {

    private EditText name;
    private EditText last_name;
    private EditText email;
    private EditText password;
    private EditText rep_password;
    private EditText phone;
    private FirebaseStorageManager firebase_storage;
    private ProgressDialog progress_dialog;
    private Account account;
    private FirebaseFirestore database;
    //private Spinner profesion;
    private Spinner spinnerServices;
    private ArrayList<Services> sData;
    private ImageView foto;


    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://aplicacion-servicios.appspot.com");

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        this.firebase_storage = new FirebaseStorageManager();
        this.database = FirebaseFirestore.getInstance();
        this.account = new Account();
        this.sData = new ArrayList<Services>();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getDataServices();
        this.spinnerServices = (Spinner) findViewById(R.id.profesion);
        this.spinnerServices.setOnItemSelectedListener(this);
    }

    public void onItemSelected(AdapterView<?> parent,View view,int position,long id){
        System.out.println(parent.getItemAtPosition(position));
    }

    public void onNothingSelected(AdapterView<?> parent){
        System.out.println(parent);
    }
    /***
     *
     * Trae los valores de los componentes.
     */
    private void getDataAccount(){
        this.name = findViewById(R.id.name );
        this.last_name = findViewById(R.id.last_name );
        this.email = findViewById(R.id.email );
        this.password = findViewById(R.id.password );
        this.rep_password = findViewById(R.id.rep_password );
        this.phone = findViewById(R.id.phone );
        //this.spinnerServices = findViewById(R.id.profesion);
    }

    /**
     *
     * Envia la informacion de la solicitud para crear una cuenta.
     */
    public void sendAccountRequest(View view){
        ProgressDialog progress_dialog =  new ProgressDialog(this);
        Context context = this;
        this.getDataAccount();

        if ( this.name.getText().toString().isEmpty() || this.last_name.getText().toString().isEmpty() || this.email.getText().toString().isEmpty() || this.password.getText().toString().isEmpty() || this.phone.getText().toString().isEmpty() || this.spinnerServices.getSelectedItem().toString().isEmpty()  ) {
            Toast.makeText( this , "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
        }

        else if ( !(this.password.getText().toString().equals(this.rep_password.getText().toString())) ){
            Toast.makeText( this , "Las contraseñas no son iguales", Toast.LENGTH_SHORT).show();
        }
        else if ( this.password.getText().toString().length() <6 ) {
            Toast.makeText( this , "La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
        }
        else if(firebase_storage.file_path != null) {
            progress_dialog.setMessage(" Cargando su información...");
            progress_dialog.show();

            account.setProfile_imagen( UUID.randomUUID().toString() );
            account.setEmail( this.email.getText().toString() );
            account.setName( this.name.getText().toString());
            account.setLast_name( this.last_name.getText().toString());
            account.setPhone( this.phone.getText().toString() );
            account.setPassword( this.password.getText().toString() );
            account.setProfesion( this.spinnerServices.getSelectedItem().toString() );

            //guardamos en la base de datos de google
            this.database.collection("account")
                    .add( this.account )
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            progress_dialog.setMessage(" Se ha creado su cuenta!! ");
                            createUser( account.getEmail(), account.getPassword() );
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progress_dialog.setMessage(" Ha habido un error al crear su cuenta!! ");
                        }
                    });

            firebase_storage.uploadImage( this.account.getProfile_imagen() ).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progress_dialog.dismiss();
                    Intent intent = new Intent( context, ServicesListActivity.class);
                    startActivity( intent );
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progress_dialog.dismiss();
                    Toast.makeText(SignUpActivity.this, "Hubo un problema al cargar la imagen", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(SignUpActivity.this, "Debe seleccionar una imagen", Toast.LENGTH_SHORT).show();
        }

    }

    /***
     *
     * Abre el sistema de archivos para seleccionar una foto
     */
    public void uploadProfilePicture(View view){

        Intent intent = new Intent( Intent.ACTION_GET_CONTENT );
        intent.setType("image/*");
        startActivityForResult( Intent.createChooser( intent , "Seleccionar imagen"), this.firebase_storage.PICK_IMAGE_REQUEST );
    }


    /**
     *
     * Esta funcion evalua si el codigo de la solicitud es igual al codigo de respuesta.
     *
     *
     */

    protected void onActivityResult( int requestCode , int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode , data);

        if ( requestCode == this.firebase_storage.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
            this.firebase_storage.file_path = data.getData();

        try{

            Bitmap bitmap = Images.Media.getBitmap( getContentResolver() , this.firebase_storage.file_path );
            foto.setImageBitmap(bitmap);

        }catch ( Exception e){
            e.printStackTrace();
        }
    }

    /**
      *
      * Este metodo es usa para crear el usuario directo en firebase para el login
      *
      *  */

    public void createUser(String email , String password)
    {
        Context context = this;
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText( context , "Falló la autenticación.",
                                    Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }

    /**
     * Obtenemos todos los servicios desde nuestro firebase
     * Inyectamos nuestra data en Spinner.
     */
    public void getDataServices(){
        ArrayList<String> sData = new ArrayList<String>();
        this.database.collection("services").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot dc :  task.getResult()){
                        sData.add(dc.get("name").toString());
                    }
                    Spinner inf = (Spinner) findViewById(R.id.profesion);
                    ArrayAdapter<CharSequence> adapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,sData);
                    inf.setAdapter(adapter);
                    System.out.println("DATA OBTENIDA");
                }
            }
        });
    }




}
