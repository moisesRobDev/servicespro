package com.example.proyectoapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ContactActivity extends AppCompatActivity {

    private FirebaseFirestore database;
    private String email_account;
    private static final int MY_PERMISSIONS = 1985;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ContactActivity contexto = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        setTitle("Contactar");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.database = FirebaseFirestore.getInstance();
        Intent intent = getIntent();
        this.email_account = intent.getStringExtra(MapsActivity.EMAIL);
        this.getAccount();
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //traemos el numero de telefono
                TextView phone      = ( TextView ) findViewById( R.id.phone );
                contexto.call(  phone.getText().toString() );
            }
        });
    }

    /*

        Este proceso trae la data de la base de datos y luego los pasa a labels

    */
    private void getAccount(){

        //traemos las referencias de los textview
        TextView email      = ( TextView ) findViewById( R.id.email );
        TextView phone      = ( TextView ) findViewById( R.id.phone );
        TextView name       = ( TextView ) findViewById( R.id.name );
        TextView last_name  = ( TextView ) findViewById( R.id.last_name );

        //hacemos la busqueda en la base de datos segun el correo enviado
        this.database.collection("account").whereEqualTo("email", this.email_account)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot document : task.getResult()) {
                                email.setText(document.getString("email"));
                                phone.setText(document.getString("phone"));
                                name.setText(document.getString("name"));
                                last_name.setText(document.getString("last_name"));
                                ImageView image_view;
                                image_view = findViewById(R.id.profile_picture );
                                Picasso.get()
                                        .load( R.drawable.default_profile_pic )
                                        .resize( 500, 500)
                                        .centerInside()
                                        .into( image_view );

                            }
                        }
                    }
                } );

    }

    /*

        este proceso es para validar los permisos para llamadas y luego proceder a realizar la llamada
     */

    public void call(String phone)
    {
        Toast toast = new Toast(this );

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {
                    toast.makeText(this, " Esta app necesita los permisos para realizar llamadas ", Toast.LENGTH_LONG);

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        MY_PERMISSIONS);

            }
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel: "+phone));
            startActivity(callIntent);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        Toast toast = new Toast(this );

        switch (requestCode) {
            case MY_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    toast.makeText(this, " Esta app necesita los permisos para realizar llamadas ", Toast.LENGTH_LONG);
                }
                return;
            }

        }
    }

}
