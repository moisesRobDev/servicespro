package com.example.proyectoapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.telephony.ServiceState;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyectoapp.Auth.FirebaseAuthManager;
import com.example.proyectoapp.Model.User;
import com.google.android.material.snackbar.Snackbar;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    // Atributos para el inicio de sesion
    private EditText email_text;
    private EditText password_text;
    // Creamos  un atributo de la propia clase
    private MainActivity this_activity_main;
    // Creamos atributo de firebase
    private FirebaseAuthManager auth_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        getDataLogin();
        initModelItems();
    }
    /***
     * Inicializamos los modelos que necesitamos
     */
     public void initModelItems(){
         this.this_activity_main = this;
         this.auth_manager =new FirebaseAuthManager();
     }
    /***
     *  Obtener valores de los componentes
     */
    public void getDataLogin(){
        this.email_text = findViewById(R.id.email_text);
        this.password_text = findViewById(R.id.password_text);
    }

    /***
    *  Realizar accion boton login
    * */

    public void logIn(View view){
        String email = this.email_text.getText().toString();
        String password = this.password_text.getText().toString();
       if(email.isEmpty() || password.isEmpty()){
            Snackbar.make(view,R.string.login_error,Snackbar.LENGTH_LONG).show();
            return;
        }
        auth_manager.login(this_activity_main,email,password, tasks -> {
                if(tasks.isSuccessful()){
                    User user = auth_manager.getCurrentUser();
                    if(user == null){
                        Snackbar.make(view,R.string.login_error,Snackbar.LENGTH_LONG).show();
                        return;
                    }else{
                        goServicesActivity();
                        return;
                    }
                }
                Snackbar.make(view,R.string.login_error,Snackbar.LENGTH_LONG).show();
        });
    }

    /***
     * Abre el formulario para registrarse o crear una cuenta
     *
     */

    public void goToSignUp(View view){
            Intent intent = new Intent(view.getContext(), SignUpActivity.class);
            startActivity(intent);
    }

    /**
    *  Pantalla para mostrar los servicios
    * */

    public void goServicesActivity(){
        Intent intent = new Intent(this_activity_main, ServicesListActivity.class);
        startActivity(intent);
        finish();
    }
}
